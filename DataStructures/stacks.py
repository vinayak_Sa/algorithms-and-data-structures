# Stacks in python using the list data structure.
# Stack is LIFO data structure. 
# Stacks as ADT - Abstract data types - Features - Mathematically model and doesnot need to know how to 
# create it. 
# Tower of hanoi, Balancing brackets.
# All the values are only inserted or accessed by the top of the stack.
# It is a collection/ list with the restriction that insertation or the deletion can only be performed 
# from the top of the stack - Only from the one side. 
# Time complexity of using the stack is O(1) - Constant operation. 

class Stack: 

	def __init__(self,stack = []):
		self.top = -1
		self.stack = stack

	def push(self, element):
		self.top = self.top + 1
		self.stack.insert(self.top, element)
	
	def pop(self):
		self.top = self.top - 1
		pass

	def peek_top(self):
		if(len(self.stack) < 1):
			print("Nothing in stack ")
			return None
		else:
			peek_val = self.stack[self.top]
			print("The top of the stack has value {}".format(peek_val))
			return peek_val



s = Stack()
s.push(12)
s.push(13)
s.push(22)
s.pop()
s.peek_top()
