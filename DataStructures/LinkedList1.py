# Linked Lists. 
# LL doesn't take contigious block of memory to store the data. 
# Linked list doesn't have ordering in the memory like list.
# Elements are linked using the pointers.
# Fist node is the head which contains the pointer to the next element.
# Getting the element is O(N). 
# Removing the element at perticular place is O(1).


class node:
	def __init__(self,data=None):
		self.data = data
		self.next = None

class linked_list:
	
	def __init__(self):
		self.head = node()

	def append(self, data):
		new_node = node()
		new_node.data = data
		cur = self.head
		while(cur.next != None):
			cur = cur.next
		cur.next = new_node

	def length(self):
		cur = self.head
		total = 0 
		while(cur.next != None):
			total = total + 1
			cur = cur.next
		print("The total elements in the linked list is {}".format(total))


	def display(self):
		elems = []
		cur = self.head 
		while(cur.next != None):
			elems.append(cur.data)
			cur = cur.next
		print("Elements are {}".format(elems))


l1 = linked_list()
l1.append(12)
l1.append(13)
l1.append(14)

