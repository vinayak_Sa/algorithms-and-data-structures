# Queues based on FIFO Data structure
# Watch video and implement it.
class Queue: 
	def __init__(self,size=10):
		self.front = -1 
		self.rear = -1 
		self.size = size
		self.queue = []

	def isEmpty(self):
		if(self.front == -1 and self.rear == -1):
			return True
		return False

	def isFull(self):
		if(self.rear > self.size):
			return True
		return False

	def enQueue(self, val):
		if(self.isFull()):
			print("Queue has reached it's limits and cannot be filled further")
			return None
		elif(self.isEmpty()):
			self.front = 0
			self.rear = 0 
		else:
			self.rear = self.rear + 1

		self.queue.insert(self.rear, val)
		print("Value added to queue")


q1 = Queue(4)
q1.enQueue(12)
q1.enQueue(13)




