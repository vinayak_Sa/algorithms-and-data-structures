class Node:
	def __init__(self, data = None):
		self.data = data
		self.left = None
		self.right = None 



class BinaryTree:
	
	def __init__(self, root=None):
		self.root = root 

	def insert(self, root, data = None):
		new_node = Node(None)
		new_node.data = data
		new_node.left = None 
		new_node.right = None


		if(root != None):
			if(data < root.data):
				if(root.left == None):
					root.left = new_node
				else:
					self.insert(root.left, data)
			elif(data > root.data):
				if(root.right == None):
					root.right = new_node
				else:
					self.insert(root.right, data)
	
		else:
			root = new_node
			return root

		print("Node inserted")

	def pre_order(self, parent):
		if(parent == None):
			return 

		print(parent.data)
		self.pre_order(parent.left)
		self.pre_order(parent.right)

	def inorder(self, parent):
		if(parent == None):
			return 
		self.inorder(parent.left)
		print(parent.data)
		self.inorder(parent.right)

	def postorder(self, parent):
		if(parent == None):
			return 
		self.postorder(parent.left)
		self.postorder(parent.right)
		print(parent.data)
		
	def deleteNode(self, parent, val):
		if(parent.data == val):
			cur = parent
		else:
			cur = parent
			prev = None
			if(val > parent.data):
				while(cur.data != val):
					prev = cur
					cur = cur.right
					print("Going on right side")
				print("Found the node with val {} with prev node of {} and next right child is ".format(cur.data, prev.data))
				
				# If the node has only one node. - left side or the right side.

				# Checking if the ride side is None but left side has value
				if(cur.right != None and cur.left == None):
					# Change the link
					cur = cur.right
					prev.right = cur 
					print("Done making changes {} with the prev value of {}".format(cur.data, prev.data))

				# Checking if the node has left side but not the right side.

				elif(cur.right == None and cur.left != None):
					cur = cur.left
					prev.left = cur
					print("Done making changes {} with the prev value of {}".format(cur.data, prev.data))

				# If it is a leaf node.
				elif(cur.right == None and cur.left == None):
					cur = None
					print("Done deleting the leaf node") 

				else: 
					# Last case has both the nodes left and right sub tree.
					pass
			else:
				print("Still node found")
		

	
b1 = BinaryTree()
root = b1.insert(None, 13)
b1.insert(root, 14)
b1.insert(root, 11)
b1.insert(root, 16)
b1.insert(root, 12)
b1.insert(root, 18)
b1.insert(root, 20)
# b1.pre_order(root)
# b1.inorder(root)
# b1.postorder(root)
b1.deleteNode(root, 20)
