# Bubble Sort O(n^2) Algorithm
# Idea: for each element Compare it element with next element.
# Most - Important. Increase the bubble.  

def bubble_sort(data):

	for i in range(0, len(data)):
		for j in range(0, len(data) - 1):
			if(data[j] > data[j + 1]):
				data[j], data[j + 1] = data[j + 1], data[j]
		print("Operation Version 1")
	return data 

# Second version. 
# Better version the complexity is same for worst case as O(N^2)
# But the best case is O(N) which is muxh better then O(N^2)

def bubble(nums):
	swap = True 
	while(swap == True):
		swap = False
		for i in range(0, len(nums) - 1):
			if(nums[i] > nums[i + 1]):
				nums[i], nums[i + 1] = nums[i + 1], nums[i]
				swap = True
		print("Operation Version 2")
	return nums

result1 = bubble_sort([3, 2, 5, 6])
result2 = bubble([3, 2, 5, 6])
print("The result with second is {}".format(result2))
