def partation(nums, l, h):
	
	pivot = nums[l]
	i = l 
	j = h 

	while(i < j):
		# Find the value less than pivot 
		while(nums[i] < pivot):
			i = i + 1

		# Value greater than the pivot.
		while(nums[j] > pivot):
			j = j - 1
		# Value greater than the pivot.
	
		# Swap the pivot.
		if(l < h):
			nums[j], nums[i] = nums[i], nums[j]
		# I am just returning the pivot index and swapped values but one thing is that the j should also be changed with the pivot ??
		# Doubt ?  
	return j


def quickSort(nums, l, h):
	# To ensure atleast there are two elements.
	if(l < h):
		j = partation(nums, l, h)
		partation(nums, j + 1, h)
		partation(nums, l, j)

	return nums

a = [3, 33, 21, 12, 10, 2]
sort = quickSort(a, 0, len(a) - 1)
print("The result of sort is {}".format(sort))