# Linear search is go through all the elements and search for element.
# Go through each element if found then true else False.

def linear_search(data, target):
	for i in data:
		if(i == target):
			return True
	return False


data = [2, 4, 6, 10, 11, 13, 14]
result = linear_search(data, 14)
if(result == True):
	print("Found the target")
else:
	print("Target not found ")
