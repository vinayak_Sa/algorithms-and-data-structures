# Sum of digits.
def digits_sum(num):
    sum = 0
    while(num!= 0):
        sum = sum + num % 10
        num = num // 10
    return sum

result = digits_sum(32)

# Smallest Number
def smallest(nums):
    smallest = nums[0]
    i = 0
    while(i < len(nums)):
        if(smallest > nums[i]):
            smallest = nums[i]
        i = i + 1
    return smallest

smallest_result = smallest([3, 12, 22, 19, 33, 1])

# Recursion problems. - Reverse a string and factorial problem.

# Factorial very important.
def factorial(num):
    if(num == 0):
        return 1

    if(num == 1):
        return num
    fact = num * factorial(num - 1)
    return fact
    
    
num = int(input('Please enter the number '))
factorial_result = factorial(num)    
    
# Anagram Problem using sorting the strings

def anagram(s1, s2):
	len1 = len(s1)
	len2 = len(s2)
	if(len1 != len2):
		return False
	sort1 = sorted(s1)
	sort2 = sorted(s2)
	if(sort1 == sort2):
		return True
	else:
		return False

result = anagram("abc", "cab")
print("The above result is {}".format(result))

# Palindrome problem.

def isPalindrome(s1):
	i = 0
	palindrome = False
	while(i <= len(s1) - 1):
		if s1[i] == s1[len(s1) - (i + 1)]:
			palindrome = True
		else:
			palindrome = False


		i = i + 1
	
	return palindrome

r = isPalindrome("racecar")
r1 = isPalindrome("bob")
print(r1)
