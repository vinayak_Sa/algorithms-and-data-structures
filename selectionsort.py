# Selection sort is also o(N^2) algorithm.
# find the smallest in list and put that in the first place.
# Proper code. 
# Worst case is same

def selectionSort(nums):
    for i in range(0, len(nums)):
        for j in range(i, len(nums)):
            smallest_i = i
            if(nums[smallest_i] > nums[j]):
                smallest_i = j
            nums[smallest_i], nums[i] = nums[i], nums[smallest_i]
    return nums

result_selectionSort = selectionSort([10, 3, 7, 1, 100, 20])
print(result_selectionSort)
