# Binary Search algorithm is very effective algorithm 
# The algorithm functions in log way complexity is O(log N) in worst case.
# Much better than the Linear Search.

# There is one constraint on binary search algorithm.
# That is the list / Array needs to be sorted.

# Steps 
# 1. Find the middle. - first + (last - first ) / 2
# 2. If the target > nums[m] - > Change the low to m + 1
# 3. Else if target < nums[m] -> Change the high to m - 1
# 4. Else there was no element in the list - So, that is the thing. 
# Haha check also before step 2 that the element is the middle one.

#  Recursive method.

#  Problem with the pseduocode.
def binarySearch(nums, target, low, high):

	if(high < low):
		return False

	middle = low + (high - low) / 2

	print("The value of middle is {}".format(middle))

	if(nums[middle] == target):
		return middle

	elif(target > nums[middle]):
		binarySearch(nums, middle + 1, high, target)

	else:
		binarySearch(nums, low, middle - 1, target)
	

data = [3, 5, 6, 7, 8]


def binarySearchI(nums, target, low, high):
	while(low <= high):
		middle = low + (high - low) / 2
		if(nums[middle] == target):
			return middle
		elif(target > nums[middle]):
			low = middle + 1 
		else:
			high = middle - 1
	return False

r = binarySearch(data, 7, 0, len(data) - 1)
print("The result of binary search is {}".format(r))



