# Insertation Sort Algo : - Worst case - O(n^2) algorithm
# Better than other algorithms as best case of this algorithm is O(N).
# Worst case and Average case of this algorithm is o(N^2)

def insertation_sort(nums):
	for i in range(1, len(nums)):
		key = nums[i] # o(N)
		j = i - 1 # o(N)
		while(j >= 0 and key < nums[j]):
			nums[j + 1] = nums[j] # O(N^2)
			j = j - 1 # O(N^2)
		nums[j + 1] = key 
	return nums

result = insertation_sort([10, 6, 12, 3])
print("The result of the insertation sort is {}".format(result))